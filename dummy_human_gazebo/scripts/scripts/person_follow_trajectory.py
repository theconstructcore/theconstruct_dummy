#!/usr/bin/env python
import sys
import rospy
import math
import tf
from geometry_msgs.msg import Twist, Pose, Point


class PersonFollowTrajectory(object):

    def __init__(self, follower_model_name="dummy_human", model_to_be_followed_name = "current_trajectory"):

        self.listener = tf.TransformListener()

        self._follower_model_name = follower_model_name
        self._model_to_be_followed_name = model_to_be_followed_name

        self.init_trajectory_array()

        self.follow_topic = "/follow_trajectory"
        self.trajector_publisher = rospy.Publisher(self.follow_topic, Pose, queue_size=10)
        

        self._cmd_vel_topic_name = self._follower_model_name+'/cmd_vel'
        self.person_vel = rospy.Publisher(self._cmd_vel_topic_name, Twist,queue_size=1)

        self.check_publishers_connection()

        self.rate = rospy.Rate(10.0)

        self.ctrl_c = False
        rospy.on_shutdown(self.shutdownhook)

        rospy.loginfo("READY")
    
    def check_publishers_connection(self):
        """
        Checks that all the publishers are working
        :return:
        """
        rate = rospy.Rate(10)  # 10hz
        while (self.trajector_publisher.get_num_connections() == 0 and not rospy.is_shutdown()):
            rospy.logdebug("No susbribers to trajector_publisher yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("trajector_publisher Publisher Connected")

        while (self.person_vel.get_num_connections() == 0 and not rospy.is_shutdown()):
            rospy.logdebug("No susbribers to person_vel yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("person_vel Publisher Connected")

        rospy.logdebug("All Publishers READY")

    
    def init_trajectory_array(self, error_pose_trajectory=0.1):

        self._error_pose_trajectory = 0.5
        p1 = Point()
        p2 = Point()
        p3 = Point()
        p4 = Point()

        p1.x = 5.01668
        p1.y = 5.01668
        p1.z = 0.1

        p2.x = -5.01668
        p2.y = 5.01668
        p2.z = 0.1

        p3.x = -5.01668
        p3.y = -5.01668
        p3.z = 0.1

        p4.x = 5.01668
        p4.y = -5.01668
        p4.z = 0.1

        self.trajectory_points_array = [p1,p2,p3,p4]

    def shutdownhook(self):
        # works better than the rospy.is_shut_down()
        print "shutdown time! Stop the robot"
        cmd = Twist()
        cmd.linear.x = 0.0
        cmd.angular.z = 0.0
        self.person_vel.publish(cmd)
        self.ctrl_c = True
    
    def update_publish_current_trajectory(self, index_value):

        current_pose = Pose()
        current_point = self.trajectory_points_array[index_value]
        current_pose.position.x = current_point.x
        current_pose.position.y = current_point.y
        current_pose.position.z = current_point.z

        current_pose.orientation.w = 1.0
        rospy.loginfo("Publishing Point Pose="+str(current_pose))
        self.trajector_publisher.publish(current_pose)

    def start_loop(self):
     
        follower_model_frame = "/"+self._follower_model_name
        model_to_be_followed_frame = "/"+self._model_to_be_followed_name       

        index = 0
        self.update_publish_current_trajectory(index)
        
        rospy.logdebug("Starting Loop...")
        while not self.ctrl_c:
            rospy.logdebug("Inside Loop...")
            trans = None
            rot = None
            try:
                (trans,rot) = self.listener.lookupTransform(follower_model_frame, model_to_be_followed_frame, rospy.Time(0))
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as inst:
                rospy.logerr(str(inst))
    
            if trans and rot:

                #angular = 1 * math.atan2(trans[1], trans[0])
                

                distance = math.sqrt(trans[0] ** 2 + trans[1] ** 2)
                # To avoid overshooting when change of distance is to high
                linear = min(0.1 * distance, 0.5)
                
                rospy.logdebug("distance form trajectory=="+str(distance))

                angular = 0.7 * math.sin(trans[1]/distance)
                rospy.logdebug("angular from trajectory=="+str(angular))

                cmd = Twist()
                cmd.linear.x = linear
                cmd.angular.z = angular

                self.person_vel.publish(cmd)

                # We check if it has arrived to the trajectory point:
                if distance <= self._error_pose_trajectory:
                    rospy.logwarn("Arrived to Trajectory point")
                    
                    rospy.sleep(3.0)
                    rospy.logwarn("Finished waiting" )
                    index += 1
                    if index > len(self.trajectory_points_array)-1 :
                        rospy.logwarn("Restarting Loop...")
                        index = 0
                else:
                    rospy.loginfo("NOT Arrived yet Trajectory point")
                
                self.update_publish_current_trajectory(index)
            
            else:
                rospy.logerr("Trans and Rotation TF not found...")
    
            rospy.logdebug("Loop...Index="+str(index))
            self.rate.sleep()
            


def main():
    rospy.init_node('tf_listener_person', log_level=rospy.INFO)
    object_follow = PersonFollowTrajectory()
    object_follow.start_loop()


if __name__ == '__main__':
    main()