#! /usr/bin/env python
import rospy
import time
import tf
from geometry_msgs.msg import Pose


class TrajectorFollow(object):

    def __init__(self):

        self._check_all_systems_ready()
        rospy.Subscriber("/follow_trajectory", Pose, self.follow_trajectory_callback)

    def follow_trajectory_callback(self, msg):        
        self.current_trajectory_pose = msg
        rospy.loginfo("New Trajectory Point Message="+str(self.current_trajectory_pose.position))

    
    def _check_all_systems_ready(self):
        self.current_trajectory_pose = None
        while self.current_trajectory_pose is None and not rospy.is_shutdown():
            try:
                self.current_trajectory_pose = rospy.wait_for_message("/follow_trajectory", Pose, timeout=1.0)
                rospy.logdebug("current_trajectory_pose READY=>"+str(self.current_trajectory_pose))
            except:
                rospy.logerr("current_trajectory_pose not ready yet, retrying...")
        rospy.logdebug("ALL SYSTEMS READY")
    
    def get_current_trajectory_pose(self):

        return self.current_trajectory_pose


def handle_current_trajectory_pose(pose_msg, robot_name, frame="/map"):
    br = tf.TransformBroadcaster()
    
    br.sendTransform((pose_msg.position.x,pose_msg.position.y,pose_msg.position.z),
                     (pose_msg.orientation.x,pose_msg.orientation.y,pose_msg.orientation.z,pose_msg.orientation.w),
                     rospy.Time.now(),
                     robot_name,
                     frame)

def publisher_of_tf():
    
    rospy.init_node('publisher_of_tf_node', anonymous=True, log_level=rospy.WARN)

    trajectory_follow_object = TrajectorFollow()
    
    # Leave enough time to be sure the Gazebo Model logs have finished
    time.sleep(1)
    rospy.loginfo("Ready..Starting to Publish TF data now...")
    
    rate = rospy.Rate(5) # 5hz
    robot_name = "current_trajectory"
    
    while not rospy.is_shutdown():

        pose_now = trajectory_follow_object.get_current_trajectory_pose()
        rospy.logdebug(pose_now.position)
        if not pose_now:
            print "The Pose is not yet"+str(robot_name)+" available...Please try again later"
        else:
            handle_current_trajectory_pose(pose_now, robot_name)
        rate.sleep()
    

if __name__ == '__main__':
    try:
        publisher_of_tf()
    except rospy.ROSInterruptException:
        pass